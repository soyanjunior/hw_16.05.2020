﻿#include <iostream>
using namespace std;

class Animal 
{
private:
    int a;

public:
    int GetA()
    {        return a;     }
    
    virtual void Voice() { cout << "Агрррррр!!!!!! \n"; }
};

class Dog : public Animal
{
private:
    int b;
public:
    int GetB()
    {         return b;     }
    void Voice() override { cout << "Гав! Гав! \n"; }
};

class Cat : public Dog
{
private:
    int c;
public:
    int GetC()
    {        return c;    }

    void Voice() override { cout << "Мяу) \n"; }
};

class LittleAnimal : public Animal
{
private:
    int d;
public:
    int GetD()
    {        return d;    }

    void Voice() override { cout << "I wonna milk! \n"; }
};

int main()
{
    setlocale(0, "Russian");
    Animal* array[4];
    array[0] = new Animal;
    array[1] = new Dog;
    array[2] = new Cat;
    array[3] = new LittleAnimal;
    for (int i = 0; i < 4; i++)
    {
        array[i]->Voice();
      
    }
}